(function() {
    'use strict';

    angular
        .module('miaApp')
        .controller('ComputationDialogController', ComputationDialogController);

    ComputationDialogController.$inject = ['$scope', '$stateParams', '$uibModalInstance', '$http', 'entity', 'Computation', 'VolumeOfInterest'];

    function ComputationDialogController ($scope, $stateParams, $uibModalInstance, $http, entity, Computation, VolumeOfInterest) {
        var vm = this;
        vm.computation = entity;
        vm.volumeofinterests = VolumeOfInterest.query({page: 0, size: 1000});

        vm.configuredComputation = {};
        
        try{
        	entity.$promise.then(function(data){
        		vm.configuredComputation = JSON.parse(data.computationConfiguration);
        	});
        }
        catch(e){
        	console.log("Create new entity --> try catch issue for undefined entity");
        }
                
        
        vm.load = function(id) {
            Computation.get({id : id}, function(result) {
                vm.computation = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('miaApp:computationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;
            vm.computation.computationConfiguration = JSON.stringify(vm.configuredComputation);
                        
            if (vm.computation.id !== null) {
                Computation.update(vm.computation, onSaveSuccess, onSaveError);
            } else {
                Computation.save(vm.computation, onSaveSuccess, onSaveError);
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        
        vm.selectedModuleName = function(){
        	return vm.computation.moduleName;
        }
        
        vm.addConfigFile = function(commandParameter, configFile){
            // TODO: check that commandParameter does not yet exist
            
            var fd = new FormData();
            fd.append('file', configFile);

            $http({
                method: 'POST',
                url: 'configservice/' + 'api/extconfig/file',
                headers: {'Content-Type': undefined},
                data: fd
            }).then(function success(response){
                var fileNameSaved = response.data;
                
                if (!vm.configuredComputation.configFiles) {
                    vm.configuredComputation.configFiles = [];
                }
                
                vm.configuredComputation.configFiles.push({
                    commandParameter: commandParameter, 
                    fileNameOriginal: configFile.name,
                    fileNameSaved: fileNameSaved
                });
            });
        }
        
        vm.removeConfigFile = function(index) {
            if (vm.configuredComputation.configFiles && index > -1) {
                var fileName = vm.configuredComputation.configFiles[index].fileNameSaved;
                
                $http({
                    method: 'DELETE',
                    url: 'configservice/' + 'api/extconfig/file',
                    params: {fileName: fileName}
                });
                
                vm.configuredComputation.configFiles.splice(index, 1);
            }
        }
        
        vm.modules = {
	        DoseComputation:
	        {
	        	moduleName: "DoseComputation",
	        	moduleOperations: ["min","mean","max"]
	        },
	        DvhCurveComputation:
	        {
	        	moduleName: "DvhCurveComputation",
	        	absolute: ["true","false"]
	        },
	        DvhDoseComputation:
	        {
	        	moduleName: "DvhDoseComputation",
	        	absoluteOutput: ["true","false"],
	        	limit: "integer",
	        	limitUnit: ["%", "cc"]
	        },
	        DvhVolumeComputation:
	        {
	        	moduleName: "DvhVolumeComputation",
	        	absoluteOutput: ["true","false"],
	        	limit: "integer",
	        	limitUnit: ["%", "Gy"]
	        },
	        VolumeComputation:
	        {
	        	moduleName: "VolumeComputation"
	        },
	        RuntimeComputation:
	        {
	            moduleName: "RuntimeComputation",
	            runtimeComputationName: "string",
	            configFiles: []
	        }
        };
	    
    }
})();
