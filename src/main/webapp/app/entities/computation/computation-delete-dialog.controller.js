(function() {
    'use strict';

    angular
        .module('miaApp')
        .controller('ComputationDeleteController',ComputationDeleteController);

    ComputationDeleteController.$inject = ['$uibModalInstance', '$http', 'entity', 'Computation'];

    function ComputationDeleteController($uibModalInstance, $http, entity, Computation) {
        var vm = this;
        vm.computation = entity;
        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        
        vm.confirmDelete = function (id) {
            
            var configFiles = [];
            if (vm.computation.moduleName == 'RuntimeComputation') {
                var computationConfiguration = JSON.parse(vm.computation.computationConfiguration);
                configFiles = computationConfiguration.configFiles;
            }
                
            Computation.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                    
                    for (var i = 0; i < configFiles.length; i++) {
                        vm.deleteConfigFile(configFiles[i].fileNameSaved);
                    } 
                });
        };
        
        vm.deleteConfigFile = function(fileName) {
            $http({
                method: 'DELETE',
                url: 'configservice/' + 'api/extconfig/file',
                params: {fileName: fileName}
            });
        }
    }
})();
