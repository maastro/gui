# Graphical User Interface #

The MIA graphical user interface is a microservice within the [Medical Image Analysis (MIA)](https://bitbucket.org/maastrosdt/binaries-and-documentation/wiki/Home) framework. It provides a web-based user interface for the configuration of MIA computations.

## Prerequisites ##

- Java 8
- 1 GB RAM
- Modern browser

## Usage ##

See [setting up a first computation](https://bitbucket.org/maastrosdt/binaries-and-documentation/wiki/Configuring%20a%20first%20computation) for more information on how to use the MIA user interface.

## Development ##

This application was generated using JHipster, documentation and help can be found at [https://jhipster.github.io](https://jhipster.github.io).

Before building this project, the following dependencies must be installed and configured on the machine:

- [Node.js](https://nodejs.org/) version 4 is used to run a development web server and build the project.
   Depending on the system, Node can be installed from source or as a pre-packaged bundle.

- After installing Node, it should be possible to run the following command to install development tools (like
[Bower](https://bower.io/) and [BrowserSync](https://www.browsersync.io/)). This is only required when dependencies change in package.json.

    ```npm install```

- [Gulp](https://gulpjs.com/) is used as a build system. Install the Gulp command-line tool globally with:

    ```npm install -g gulp```

    Run the following commands in two separate terminals to create a development experience where the browser
    auto-refreshes when files change on your hard drive.

    ```./mvnw gulp```

Bower is used to manage CSS and JavaScript dependencies used in this application. Dependencies can be upgraded by
specifying a newer version in `bower.json`. You can also run `bower update` and `bower install` to manage dependencies.
Add the `-h` flag on any command to see how to use it. For example, `bower update -h`.

## Building for production ##

To optimize the mia client for production, run:

   ```./mvnw -Pprod clean package```

This will concatenate and minify CSS and JavaScript files. It will also modify `index.html` so it references
these new files.

To ensure everything worked, run:

    java -jar target/*.war --spring.profiles.active=prod

Then navigate to [http://localhost:8080](http://localhost:8080) in your browser.
